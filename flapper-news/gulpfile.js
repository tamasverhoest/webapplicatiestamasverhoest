const gulp = require('gulp');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

const sass = require('gulp-sass');
const cssmin = require('gulp-minify-css');

const del = require('del');

gulp.task('clean', () => {
  return del('dist/**/*');
});


gulp.task('scripts', () => {
  return gulp.src('public/javascripts/*.js')
  .pipe(uglify())
  .pipe(concat('main.min.js'))
  .pipe(gulp.dest('dist/scripts'));
});

gulp.task('styles', () => {
  return gulp.src('public/stylesheets/style.css')
  .pipe(sass())
  .pipe(cssmin())
  .pipe(gulp.dest('dist/styles'));
});


gulp.task('default', gulp.series('clean',
 gulp.parallel('styles',
    gulp.series('scripts')), (done) => {
      gulp.watch('public/stylesheets/*.css', gulp.parallel('styles'));
      gulp.watch('public/javascripts/*.js', gulp.series('scripts')); //watches for changes
      done();
    }));


// const jshint = require('gulp-jshint');

/*

gulp.task('lint', () => {
    return gulp.src(['public/**
    /*.js', '!public/stylesheets/*.css'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'));
});
*/
